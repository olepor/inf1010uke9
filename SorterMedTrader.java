import java.util.*;

class SortIntArrayWithThreads implements Runnable {

    private int[] array;
    int from, to;

    SortIntArrayWithThreads(int[] array, int from, int to) {
        this.array = array;
        this.from = from;
        this.to = to;
    }

    public void run() {
        sort(from, to);
    }

    public synchronized void sort(int from, int to) {
        // TODO implement whichever sort you like
    }

    public void swap(int a, int b) {
        int tmp = array[a];
        array[a] = b;
        array[b] = tmp;
    }

}


class SortMain {

    public static void main(String[] args) {
        int[] array = new int[100];
        // populate the arrat
        Random r = new Random();
        for (int i=0; i<100; i++) {
            array[i] = r.nextInt(100);
        }

        Thread t1 = new Thread(new SortIntArrayWithThreads(array, 0, array.length/2));
        Thread t2 = new Thread(new SortIntArrayWithThreads(array, array.length/2 +1, array.length));
    }
}
