
class ThreadName implements Runnable {

    public void run() {
        System.out.println(Thread.currentThread().getName());

    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new ThreadName());
        Thread t2 = new Thread(new ThreadName());
        t1.setName("First Thread");
        t2.setName("second Thread");
        t1.start();
        t2.start();
    }

}
