import java.util.*;
import java.io.*;

class WordsInFileCounter extends Thread {

    public static volatile int nrOfWords = 0;
    public File f;
    Scanner in;

    WordsInFileCounter(File f) {
        this.f = f;
    }

    public void run() {
        try {
        in = new Scanner(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (in.hasNextLine()) {
            String line = in.nextLine();
            nrOfWords += line.split(" ").length;
        }
    }
}


class NumberOfWordsInFiles {


    public static void main(String[] args) {
        String searchWord = args[0];
        ArrayList<File> files = new ArrayList<>();
        Scanner in;
        int nrOfWords;
        String line;

        for (int i=0; i<args.length; i++) {
            files.add(new File(args[i]));
        }

        WordsInFileCounter t;
        for(File file: files) {
            t = new WordsInFileCounter(file);
        }
        while (Thread.activeCount() != 0) {};
        System.out.println("Nr of words is: " + w.nrOfWords);

    }
}


