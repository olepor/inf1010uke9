class Counter implements Runnable {

    private int c = 0;

    public void run() {
        for (int i=0; i<2000; i++) {
            increment();
        }
        for (int i=0; i<2000; i++) {
            decrement();
        }
    }

    public void increment() {
        c++;
    }

    public void decrement() {
        c--;
    }

    public int value() {
        return c;
    }

    public static void main(String[] args) {
        Counter cnt = new Counter();
        (new Thread(cnt)).start();
        (new Thread(cnt)).start();
        System.out.println(cnt.value());
    }

}
